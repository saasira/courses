package com.example.courses.io;

import java.util.concurrent.ThreadLocalRandom;

public class Stringer {
	
	public static final String ALNUM = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
	public static String random( int length ) {
        int L=ALNUM.length();
        ThreadLocalRandom random=ThreadLocalRandom.current();
        StringBuilder builder = new StringBuilder( length );
        for( int i = 0; i < length; i++ ) {
            builder.append( ALNUM.charAt( random.nextInt(L) ) );
        }
        return builder.toString();
    }
    
}

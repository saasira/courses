package com.example.courses.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextHandler {
	
	private void writeTextFile() throws IOException {
		File test = new File("E:\\Temp\\test.txt");
		
		if(!test.exists()) {
			test.createNewFile();
		}
		
		FileWriter writer = new FileWriter(test, true);
		for(int i=0;i<100;i++) {
			writer.write(Stringer.random(32) + "\n");
		}
		writer.flush();
		writer.close();				
	}
	
	private void readTextFile() throws IOException {
		File test = new File("E:\\Temp\\test.txt");
		
		if(!test.exists()) {
			throw new IOException("no such file : "+ test.getName());
		}
		
		FileReader reader = new FileReader(test);
		
		char[] buffer = new char[4* 1024]; 
		while(reader.read(buffer)!=-1) {
			System.out.println(new String(buffer)); 
		}
		reader.close();				
	}
	
	private void writeTextFileWithBuffering() throws IOException {
		File test = new File("E:\\Temp\\test.txt");
		
		if(!test.exists()) {
			test.createNewFile();
		}
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(test, true));
		for(int i=0;i<100;i++) {
			writer.write(Stringer.random(32) + "\n");
		}
		writer.flush();
		writer.close();				
	}
	
	private void readTextFileWithBuffering() throws IOException {
		File test = new File("E:\\Temp\\test.txt");
		
		if(!test.exists()) {
			throw new IOException("no such file : "+ test.getName());
		}
		
		BufferedReader reader = new BufferedReader(new FileReader(test));
		
		char[] buffer = new char[4* 1024]; 
		while(reader.read(buffer)!=-1) {
			System.out.println(new String(buffer)); 
		}
		reader.close();	
	}
	
	
	public static void main(String[] args) {
		TextHandler handler = new TextHandler();
		try {
			handler.writeTextFile();
		} catch(Exception exception) {
			exception.printStackTrace(System.out); 
		}
		
		try {
			handler.readTextFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		
		try {
			handler.writeTextFileWithBuffering();
		} catch(Exception exception) {
			exception.printStackTrace(System.out); 
		}
		
		try {
			handler.readTextFileWithBuffering();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}

	}

}

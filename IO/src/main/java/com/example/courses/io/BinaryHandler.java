package com.example.courses.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class BinaryHandler {

	private void writeBinaryFile() throws IOException {
		File test = new File("E:\\Temp\\test.bin");
		
		FileOutputStream fos = new FileOutputStream(test);
		for(int i=0;i<100;i++) {
			fos.write((Stringer.random(32) + "\n").getBytes(StandardCharsets.UTF_8)); 
		}
		fos.flush();
		fos.close();	
	}
	
	private void readBinaryFile() throws IOException {
		File test = new File("E:\\Temp\\test.bin");
		
		if(!test.exists()) {
			throw new IOException("no such file : "+ test.getName());
		}
		
		FileInputStream fis = new FileInputStream(test);
		
		byte[] buffer = new byte[4* 1024]; 
		while(fis.read(buffer)!=-1) {
			System.out.println(new String(buffer, StandardCharsets.UTF_8)); 
		}
		fis.close();				
	}
	
	private void writeBinaryFileWithBuffering() throws IOException {
		File test = new File("E:\\Temp\\test.bin");
		
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(test));
		for(int i=0;i<100;i++) {
			bos.write((Stringer.random(32) + "\n").getBytes(StandardCharsets.UTF_8)); 
		}
		bos.flush();
		bos.close();	
	}
	
	private void readBinaryFileWithBuffering() throws IOException {
		File test = new File("E:\\Temp\\test.bin");
		
		if(!test.exists()) {
			throw new IOException("no such file : "+ test.getName());
		}
		
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(test));
		
		byte[] buffer = new byte[4* 1024]; 
		while(bis.read(buffer)!=-1) {
			System.out.println(new String(buffer, StandardCharsets.UTF_8)); 
		}
		bis.close();				
	}
	
	public static void main(String[] args) {
		BinaryHandler handler = new BinaryHandler();
		
		try {
			handler.writeBinaryFile();
		} catch(Exception exception) {
			exception.printStackTrace(System.out); 
		}
		
		try {
			handler.readBinaryFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}
		
		try {
			handler.writeBinaryFileWithBuffering();
		} catch(Exception exception) {
			exception.printStackTrace(System.out); 
		}
		
		try {
			handler.readBinaryFileWithBuffering();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(System.out);
		}

	}

}


package com.example.courses.collections;

public class Employee extends Person {

	private Long id;
	
	protected Short department;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id=id;
	}

	public Short getDepartment() {
		return this.department;
	}
	
	public void setDepartment(short department) {
		this.department=department;
	}
}

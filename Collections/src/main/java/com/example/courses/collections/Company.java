package com.example.courses.collections;

import java.util.Set;
import java.util.Comparator;
import java.util.HashSet;

public class Company implements Comparable<Company> {

    private String id;

    private String name;

    private Set<Employee> employees;
    
    private Set<Department> departments;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public Set<Department> getDepartments() {
        return this.departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments=departments;
    }

    public void addDepartment(Department department) {
        if(this.departments==null) {
            this.departments = new HashSet<>();
        }
        this.departments.add(department);
    }

    public void removeDepartment(Department department) {
        if(this.departments==null) {
            return;
        }
        this.departments.remove(department);
    }

	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", employees=" + employees + ", departments=" + departments
				+ "]";
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Company o) {
		if(o==null || o.name==null || o.name.isEmpty()) {
			return 1;
		}
		return this.name.compareTo(o.name);
	}
	
	

    
}

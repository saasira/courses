package com.example.courses.collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ROC {

	private static Set<Company> companies;
	
	private ROC() {}
	
	public static Set<Company> getCompanies() {
        return ROC.companies;
    }

    public static void setCompanies(Set<Company> companies) {
        ROC.companies=companies;
    }

    public static void registerCompany(Company company) {
        if(ROC.companies==null) {
            companies = new HashSet<>();
        }
        ROC.companies.add(company);
    }

    public static void closeCompany(Company company) {
        if(ROC.companies==null) {
            return;
        }
        ROC.companies.remove(company);
    }
    
	public static void main(String[] args) {
		Company dabba = new Company();
		dabba.setName("Dabba Services Private Limited");
		dabba.setId("UIN125XDF193YZ9736GHFK");
		
		Employee c1e1 = new Employee();
		c1e1.setId(1);
		c1e1.setName("1");
		c1e1.setAge((byte)1); 
		ROC.registerCompany(dabba);
		
		Company chemcha = new Company();
		chemcha.setName("Chemcha Services Private Limited");
		chemcha.setId("UIN125G5632YZ9736GHFK");
		ROC.registerCompany(chemcha); 
		
		Company bucket = new Company();
		bucket.setName("Bucket Manufacturers Private Limited");
		bucket.setId("UIN125XDF193YZ9736GHFK");
		ROC.registerCompany(bucket); 
		
		for(Company c : ROC.companies) {	//for(int i=0; i<ROC.companies.size();i++) { Company c = ROC.companies.get(i);
			System.out.println(c); 
		}
		
		Iterator<Company> iterator = ROC.companies.iterator();
		Company company;
		while(iterator.hasNext()) {
			company = iterator.next();
			System.out.println(company); 
		}
		
		
		
	}

}

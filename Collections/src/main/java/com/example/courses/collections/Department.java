package com.example.courses.collections;

import java.util.Set;
import java.util.HashSet;

public class Department {

    private String name;

    private short id;

    private Set<Long> employees;

    public short getId() {
        return this.id;
    }

    public void setId(short id) {
        this.id=id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public Set<Long> getEmployees() {
        return this.employees;
    }

    public void setEmployees(Set<Long> employees) {
        this.employees = employees;
    }

    public void addEmploye(Long id) {
        if(this.employees==null) {
            this.employees=new HashSet<>();
        }
        this.employees.add(id);
    }

    public void removeEmployee(Long id) {
        if(this.employees==null) {
            return;
        }
        this.employees.add(id);
    }

}

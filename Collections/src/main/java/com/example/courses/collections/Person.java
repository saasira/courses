package com.example.courses.collections;

public class Person {
	
	private String name;
	
	private byte age;
	
	private short height;
	
	private short weight;
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public byte getAge() {
		return this.age;
	}
	
	public void setAge(byte age) {
		if(this.age>126) {
			throw new IllegalArgumentException("no human can be so old");
		}
		this.age=age;
	}
	
	public short getHeight() {
		return this.height;
	}
	
	public void setHeight(short height) {
		if(this.height>200) {
			throw new IllegalArgumentException("no human can be so tall");
		}
		this.height=height;
	}
	
	public short getWeight() {
		return this.weight;
	}
	
	public void setWeight(short weight) {
		if(this.weight>200) {
			throw new IllegalArgumentException("no human can be so heavy");
		}
		this.weight=weight;
	}
	
}